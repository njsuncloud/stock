## 平台简介

* 本系统包含物料、仓库模块，适用于传统制造业中小企业仓库管理需求的使用，快速导入物料期初库存，设置仓库信息、支持批次号、货位、安全库存。
* 采用前后端分离的模式，可切换为微服务版本。
* 后端采用Spring Boot、Spring Cloud、Jwt 。
* 前端采用Vue、Element ui，权限认证使用Redis。

## 行业应用

* 系统适用于机械制造行业，包括整机生产、零配件生产。实现从研发BOM设计、物料需求计算、采购入库，到车间排产、完工、报工的完整业务流程。
* 系统适用于电子产品行业、仪器仪表、电子制造等，提供研发BOM的可替换物料、同颜色物料功能。
* 升阳云ERP制剂版，源于农药生产企业的需求，提供从研发配方管理、计划管理到生产的全生命周期业务流程。

## 架构图

<img src="https://gitee.com/njsuncloud/stock/raw/master/image/erp-0-1556x935.png" width="800px"/>

## 内置功能
1.  料品档案：物料分类、物料基本档（物料导入、导出和属性）
2.  仓库管理：仓库列表、期初库存、入库单据、出库单据、月末结存、现存量、安全库存
3.  入库管理：采购入库、生产入库、委外入库、其它入库
4.  出库管理：销售出库、生产出库、委外出库、其它出库
5.  退料退货：采购退货、销售退货、生产退料、委外退料
6.  盘点调拨：盘盈入库、盘亏出库、调拨入库、调拨出库
7.  委外管理：委外生产商、委外订单、委外发料单、委外到货单
8.  统计报表：现存量明细表、现存量汇总表、出入库明细、采购入库明细表、生产入库明细表等
9.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
10.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
11.  岗位管理：配置系统用户所属担任职务。
12.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
13.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
14.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
15.  参数管理：对系统动态配置常用参数。
16. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
17. 系统接口：根据业务代码自动生成相关的api接口文档。
18. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

## 在线体验

- 演示地址：http://jb.njrsun.com
- 账号：njrsun/123

## 视频讲解
 
- 本视频完整的讲解系统的全流程，片长35分钟，建议电脑端观看并打开声音。
- 视频地址：http://v.njrsun.com/sv/2c44cfee-17f25dd6978/2c44cfee-17f25dd6978.mp4

## 系统截图

<img src="https://gitee.com/njsuncloud/stock/raw/master/image/i-mac-1102x620.png" width="800px"><br>
<img src="https://gitee.com/njsuncloud/stock/raw/master/image/erp-1.png" width="800px"><br>
<img src="https://gitee.com/njsuncloud/stock/raw/master/image/erp-3.png" width="800px"/><br>
<img src="https://gitee.com/njsuncloud/stock/raw/master/image/erp-4.png" width="800px"/><br>
<img src="https://gitee.com/njsuncloud/stock/raw/master/image/erp-5.png" width="800px"/><br>
<img src="https://gitee.com/njsuncloud/stock/raw/master/image/datav.jpg" width="800px"/><br>


## 商业授权

* 针对企业工厂提供源码授权服务，针对软件公司和个人提供合作业务服务。
* 本系统针对中小企业提供SAAS服务，符合国家税务认定的小微企业享受共享云价格服务，更多产品信息可以访问升阳云 http://www.njrsun.com
